sbt_pkt_item;
<'
-- Whether this packet has good parity
type sbt_pkt_kind_t: [GOOD, BAD];
-- A control-knob type to affect the relative packet size.
type sbt_pkt_size_t: [SHORT, LONG, WHO_CARES];

-- Basic data-item for the input and output interfaces 
-- of the router DUT.
-- This data-item must be derived from "any_sequence_item"
struct sbt_packet_s like any_sequence_item  {
    -- Constrain to create GOOD or BAD parity packets.
    -- There is a soft constraint on this field to GOOD.
    -- Be sure to do a soft_reset() here if you want it random. 
    pkt_kind    : sbt_pkt_kind_t;
    -- Constrain to affect the "len" field (# of data bytes) for the packet
    pkt_size    : sbt_pkt_size_t;
    //payload_size: sbt_size_t;
    -- Sent into the DUT
    %addr     : uint(bits: 2);
    -- as described by the spec
    %len      : uint(bits: 6);
    -- The number of byte must be between 1 nand 63
    %data[len]: list of byte;
    -- This value will depend on the pkt_kind field. 
    %parity   : byte;

    -- Can be used by the BFM or driver to pace the delay between 
    -- packets being sent to the DUT.   Defaults to 1..5.
    pkt_delay: byte;
    keep soft pkt_delay in [1..5]; -- using  0..10 hits a DUT bug   
 
    -- This field should be constrained to the maximum possible
    -- value that the "addr" field can have for a given DUT.
    -- The current SBT router is limited to 3 output ports, but a 
    -- future version may have up to 4, since the addr field is 2-bits:
    max_addr: uint(bits: 2);
    keep soft max_addr == 2;

    keep addr <= max_addr;
    keep gen (max_addr) before (addr);

    -- This implementation does not allow 0-length packets
    keep soft len  != 0;

    when SHORT'pkt_size sbt_packet_s {
      keep len < 10;
    };
    when LONG'pkt_size sbt_packet_s {
      keep len > 20;
    };

    when GOOD'pkt_kind sbt_packet_s {
      keep parity == parity_calc(addr, len, data);
    };
    when BAD'pkt_kind sbt_packet_s {
      keep parity != parity_calc(addr, len, data);
    };

    keep soft pkt_kind == GOOD;

    post_generate() is also {
      --messagef(LOW, "%s %4s %d %4s %d\n", me, pkt_kind, addr, pkt_size, len);
    };

    -- Calculates "correct" parity for the packet.
    parity_calc(in_addr: uint(bits:2), in_len:uint(bits:6), in_data: list of byte): byte is {
      result = %{in_len, in_addr};
      for each (d) in in_data {
        result = result ^ d;
      };
    };

    -- Returns whether the packet has bad parity (bad=TRUE)
    has_bad_parity(): bool is {
      result = (parity != parity_calc(addr, len, data));
    };

    -- Sets some field's values that are not physical (virtual).
    -- Useful to call this method on a pakcet-instance that was
    -- just unpacked before collecting coverage data on that instance.
    set_virtual_fields() is {
      if  has_bad_parity() {pkt_kind = BAD;
      } else {              pkt_kind = GOOD};
    };
 
    -- Coverage collection event. Should be emitted by the monitor
    -- that unpacks the struct.
    event detected_e;
    cover detected_e is {
      item pkt_kind;
      item len  using ranges = {range( [1..9 ],"SHORT" );
                                range([10..20],"MIDDLE");
                                range([21..63],"LONG"  );
                               };
      -- The ignore option will have to be overridden if the max_addr value is not 2.
      item addr using ignore = (addr == 3);
    };    
};
'>

