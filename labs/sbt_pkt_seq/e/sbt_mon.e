<'
import sbt_mon_h;
extend sbt_mon_u {

   !data_iop    : inout simple_port of byte;
   !valid_iop   : inout simple_port of bit;
   !suspend_iop : inout simple_port of bit;
   !error_iop   : inout simple_port of bit;

   max_cycles_between_pkts: uint;
   keep soft max_cycles_between_pkts == 1000; 

   when ROUTER_IN sbt_mon_u {
      -- Declare events bad_parity and err_rise
      event bad_parity_e is true(pkt.has_bad_parity() )@pkt_detected_e;
      event err_rise_e is rise(error_iop$)@clock_e;
      event suspend_data_rise_e is rise(suspend_iop$)@clock_e;
      event suspend_data_fall_e is fall(suspend_iop$)@clock_e;
   };
    
   when ROUTER_IN do_checking sbt_mon_u {
      -- "If a packet is driven to the SBT with bad parity, expect that the
      -- output signal err occurs 1 to 16 cycles after packet end"
      expect @bad_parity_e => {[0..15]; @err_rise_e} @clock_e else
        dut_error(short_name_path(), ": A bad parity packet was sent to SBT ",
                  "but the err signal was not asserted.");

      -- "When the SBT DUT signal, suspend_data_in rises, it must fall within
      -- 100 clock cycles."
      expect @suspend_data_rise_e => {[0..99]; @suspend_data_fall_e} @clock_e else 
        dut_error(short_name_path(), ": The suspend signal was asserted for more than 100 clocks.");

      out_pkt(pkt: sbt_packet_s, name: sbt_env_name_t, port_num: byte) is only { 
         out_pkt_omp$(pkt, name, port_num);
      };
   };

   when ROUTER_OUT sbt_mon_u {     
      event vld_chan_rise_e is rise(valid_iop$)@clock_e;
      event suspend_fall_e is fall(suspend_iop$)@clock_e;  
      event vld_channel_and_suspend_low_e is true( (valid_iop$   == 1) and
                                                 (suspend_iop$ == 0) )@clock_e;

      -- Use @z to check if any bits in data signal are high-z
      event data_has_x_or_z_e is true(data_iop.has_x() or data_iop.has_z() )@clock_e;
  };

  when ROUTER_OUT do_checking sbt_mon_u {
     -- "When the SBT DUT signal, vld_channel_x rises, the input signal,
     -- suspend_x should fall within 30 clock cycles."
     expect @vld_chan_rise_e => {[0..29]; @suspend_fall_e}@clock_e else
         dut_error(short_name_path(), ": The suspend",
         " signal did not fall within 30 cycles of the rise of vld_channel");
    
     -- "When the SBT DUT signal, vld_chan_x is asserted (high),
     -- and the "suspend_x" is also unasserted (low), the output
     -- DUT channelx should not be tri-stated (high-z) or unkown."
     expect not(@data_has_x_or_z_e) @vld_channel_and_suspend_low_e else
         dut_error(short_name_path(), ": The data had a tri-state or unkown ",
         "value during the time that vld_channel was asserted.");

     out_pkt(pkt: sbt_packet_s, name: sbt_env_name_t, port_num: byte) is only { 
        out_pkt_omp$(pkt, name, port_num);
     };
  };

   !lob: list of byte;
   run() is also {
     lob.clear();
   };

   watch_bus() @clock_e is also {
     while TRUE {
       sync true(valid_iop$ == 1);
       while (valid_iop$ == 1) {
         sync true(suspend_iop$ == 0);
         lob.push(data_iop$);         
         wait [1];
       };
       lob.push(data_iop$);
       try {unpack(packing.low, lob, pkt);
           } else { 
            message(LOW, "Uh-oh!") {print lob using HEX;};
            dut_error("Bad unpack"); 
           };
       lob.clear();       
       pkt.set_virtual_fields();
       out_pkt(pkt, port_name, port_num);
       emit pkt_detected_e;
     };
   };

   check() is also {
     messagef(LOW, "Processed %5d packets", num_pkts);
     check that lob.is_empty() else dut_error(short_name_path(), 
                    ": Test ended before a pkt was fully processed.");
   };

   !num_pkts: uint;
   on pkt_detected_e {
     num_pkts = num_pkts + 1;
     if (get_objection_counter(TEST_DONE) == 0) {
       raise_objection(TEST_DONE);
       start timer(max_cycles_between_pkts);
     };
     if (get_coverage) {
       emit pkt.detected_e;
     };
   };
   timer(cycles: uint) @clock_e is {
     wait [cycles];
     drop_objection(TEST_DONE);
   };

};
'>
