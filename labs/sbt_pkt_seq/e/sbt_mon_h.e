<'
-- This unit will only passively sample the signals from the router DUT.
-- It will collect interface activity and create a packet instance to
-- provide to the data checker. It will also collect coverage information
-- and perform temporal checks on the interface to which it is associated.
unit sbt_mon_u like sbt_phy_u {
   -- struct that will contain info about activity on the interface.
   !pkt: sbt_packet_s;

   -- Emitted when the monitor unpacks a struct.
   event pkt_detected_e;

   -- Connect this to a DUT clock.
   event clock_e;                            

   -- Whether this monitor should enable coverage collection.
   -- It determines whether the coverage event of the "unpacked
   -- instance of sbt_packet_s" will be emitted. Default is TRUE.
   get_coverage: bool;
   keep soft get_coverage;
   -- Whether this monitor should perform (temporal) checking. 
   -- The enclosing unit determines whether the monitor provides
   -- data-items to a data-checker via a method port. Default is TRUE.
   do_checking:  bool;
   keep soft do_checking;
   
   -- The "hook" to provide information to a (optional) data-checker.
   out_pkt_omp: out method_port of pkt_pipe is instance;
   keep bind(out_pkt_omp, empty); 

   -- An empty local method with same arguments as out_pkt_omp.
   -- This will be implemented later.
   out_pkt(pkt: sbt_packet_s, name: sbt_env_name_t, port_num: byte) is { };

   -- The main loop that runs forever to watch over the interface -- to which this monitor is bound.
   watch_bus() @clock_e is {};

   when ROUTER_OUT sbt_mon_u {
     -- IF NEEDED: Use watch_bus() @clock_e is only ... HERE
   }; 
   when ROUTER_IN  sbt_mon_u {
     -- IF NEEDED: Use watch_bus() @clock_e is only ... HERE
   };

   run() is also {
     start watch_bus();
   };

};
'>
