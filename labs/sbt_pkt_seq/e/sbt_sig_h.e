<'
-- This is the signal map unit that will contain the RTL signal-names
-- of the DUT interface for  which it is meant.
unit sbt_sig_u like sbt_phy_u {
  -- No need to really edit this file yet.
  -- Take note that it like-inheriting from sbt_phy_u.
  when ROUTER_IN sbt_sig_u {
    --Place signal names here for the router INput signals
  };

  when ROUTER_OUT sbt_sig_u {  
    --Place signals names here for the router OUTput signals
  };
};
'>
