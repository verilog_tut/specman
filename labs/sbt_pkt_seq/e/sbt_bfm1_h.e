<'
-- This unit will drive the interface signals of the router DUT.
-- It will get an instance of a packet it is to drive from the sequence driver.
unit sbt_bfm1_u like sbt_phy_u {
  -- Connected to the clock of the physical interface of the DUT.
  event clock_e;                           

  -- Runs forever. Gets as many sbt_packet_s data-items as seq driver will provide.
  -- This method will be implemented in the sbt_bfm1.e file 
  drive_bus() @clock_e is {};

  -- This method will be implemented in the file that "connects" the bfm to the seq-driver.
  get_pkt(): sbt_packet_s @clock_e is {};

  -- This method will be implemented in the file that "connects" the bfm to the seq-driver.
  its_done()  is {};

  //Send in packet using the DUT protocol 
  -- This method will pack the provided pkt instance into a list
  -- byte and drive it into the router according to the DUT protocol.
  send_pkt(in_pkt: sbt_packet_s) @clock_e is {};
  
  run() is also {
    start drive_bus();
  };
};
'>
