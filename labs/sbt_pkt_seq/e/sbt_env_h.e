<'
-- Top-level env for the SBT router DUT.
-- This env instances the necassary agents for testing the router.
unit sbt_env_u like any_env {
  -- Standard name field per the eRM convention.
  name: sbt_env_name_t;
  keep name == ROUTER;
  
  -- The synchronizer unit.  Signal (ports) to be used 
  -- throughout this env/design are placed in this unit.  
  -- Physical 'e' objects that need this signal/port information
  -- should use references to ports in this unit.
  sync: sbt_sync_u is instance;

  -- Instance of a unit that will be associated with an HDL interface.
  -- This unit will instance a BFM, a monitor, and a sequence driver.
  -- This is the ROUTER_IN agent.
  agent: ROUTER_IN sbt_agent_u is instance;
  keep agent.parent == me; 
  keep agent.port_num  == 0;
  
  -- Instance of a unit that will be associated with an HDL interface.
  -- This unit will instance a BFM, a monitor, and a sequence driver.
  -- This is the list of ROUTER_OUT agents.  Defaults to a list.size() of 3.
  agents: list of ROUTER_OUT sbt_agent_u is instance;
  keep soft agents.size() == 3;
  keep for each (a) in agents {
    a.port_num  == index;
    a.parent    == me;
  };

  -- The instance of the data-checker.  
  checker: sbt_checker_u is instance;

  -- Standard eRM method. Used by message()/messagef()
  short_name(): string is also {
    result = name.to_string();
  };
};

extend sbt_agent_u {
  -- Reference to the env that instances this agent.
  parent: sbt_env_u;
};

'>
