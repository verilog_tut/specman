<'
import sbt_checker_h;
extend sbt_checker_u {   

  -- Implement got_pkt_imp(). It should just call got_pkt() with its arguments
  got_pkt_imp(pkt: sbt_packet_s, name: sbt_env_name_t, port_num: byte) is only {
    got_pkt(pkt, name, port_num);
  };

  check() is also {
    messagef(LOW, "Scoreboard checked %d packets", num_pkts);
    if !scoreboard.is_empty() {
      message(LOW, scoreboard);
      dut_error("Test ended and there are remaining items on the scoreboard (see list above)");
    };
  };

  run() is also {
    scoreboard.clear();
  };


  !num_pkts: uint; 
  got_pkt(pkt: sbt_packet_s, name: sbt_env_name_t, port_num: byte) is only {
     if (name == ROUTER_IN)  {
     --outf("RCV: %02x %02d %02x\n", pkt.addr, pkt.len, pkt.parity);
       scoreboard.push(deep_copy(pkt)); 
       num_pkts = num_pkts + 1;
     };
     if (name == ROUTER_OUT) {
       var i: uint;
       i = scoreboard.first_index(.addr == port_num);
       if (i == UNDEF) {
         message(LOW, "Expected packets: ", scoreboard);
         print pkt;
         dut_error("Cannot find packet in the expected list for the packet shown above.");
       } else {
         var exp_pkt: sbt_packet_s;
         exp_pkt = scoreboard[i];
         var diff: list of string = deep_compare_physical(exp_pkt, pkt, 10);
         if (diff.size() == 0) {
           scoreboard.delete(i);
         --outf("OK : %02x %02d %02x\n", pkt.addr, pkt.len, pkt.parity);
         } else {
           dut_error("Expected-packet value is on the left:\n ", diff);
         };
       };    
     };
  };
};

'>
