Anything outside of the  "<'" code delimiters
is ignored.         
<'
-- sbt_lab02 
type sbt_pkt_size_t: [SHORT,LONG,WHO_CARES];
struct sbt_packet_s {
    pkt_size : sbt_pkt_size_t;
    addr : uint(bits:2);
    len  : uint(bits:6);
    data[len] : list of byte;
    parity : byte;
    keep (pkt_size == SHORT) => len < 10;
    keep (pkt_size == LONG) => len > 20;
    keep addr != 3;
    keep len > 0;
}
'>
