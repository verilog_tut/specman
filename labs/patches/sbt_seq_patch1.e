<'
//A patch to work around a known Specman-issue when using 
//"trace seq" or "wave seq" commands
//See related SR issue: 40812724 , related PCRs# 4029153 & 4028932
#ifndef SPECMAN_VERSION_6_2_OR_LATER {
  #ifdef SPECMAN_VERSION_6_1_OR_LATER {
    import patches/stripe_deb.e;
  };
};
'>
