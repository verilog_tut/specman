--****                                                                ****
--****                         waveforms                              ****
--****                                                                ****
--
--                _   _   _   _   _   _   _   _   _   _   _   _   _   _   
--clock ...... : | |_| |_| |_| |_| |_| |_| |_| |_| |_| |_| |_| |_| |_| |_ 
--               :   :   :   :   :   :   :   :   :   :   :   :   :   :   :
--                ___________________             _______________
--packet_valid : /                   \___________/               \___________
--               :   :   :   :   :   :   :   :   :   :   :   :   :   :   :   
--                                        ___                         ___
--err ........ : ________________________/   \_______________________/   \___
--               :   :   :   :   :   :   :   :   :   :   :   :   :   :   :
--                ___ ___ __...__ ___ ___         ___ ___ __...__ ___
--data_in .... : X_H_X_D_X__...__X_D_X_P_>_______<_H_X_D_X__...__X_P_>_______
--               :   :   :   :   :   :   :   :   :   :   :   :   :   :   :
--                _______________________         ___________________
--packet ..... : <______packet_0_________>-------<______packet_1_____>-------
--               :   :   :   :   :   :   :   :   :   :   :   :   :   :   :
--
--H = Header
--D = Data
--P = Parity
-- 
-- the router assert vld_chan_x  when valid data appears in channel queue x
-- assert input read_enb_x to read packets from the queue.
-- receiver must keep track of packet extent and size.
-- err is asserted if parity error is detected at the end of packet reception 

--*******************************************************************************/
library IEEE;
use     std.standard.all;
use     IEEE.std_logic_1164.all;
use     IEEE.std_logic_unsigned.all;


entity fifo is port 
		 (clock:          in std_logic;
		  reset:          in std_logic;
		  write_enb:      in std_logic;
		  read_enb:       in std_logic;
		  data_in:        in std_logic_vector (7 downto 0);
		  data_out:       out std_logic_vector (7 downto 0) := "00000000";
		  empty:          inout std_logic := '1';
		  full:           inout std_logic := '0');
end fifo;

architecture behave of fifo is
  type mem_type is array(0 to 15) of std_logic_vector (7 downto 0);
  signal write_ptr, read_ptr: integer := 0;      
begin

  fifo_core: process( clock )
    variable ram: mem_type; 
    variable w_ptr, r_ptr: integer := 0;
  begin
    if (clock'event and clock = '1' and reset = '1') then
       data_out <= "00000000";
       empty    <= '1';
       full     <= '0';
       w_ptr    := 0;
       r_ptr    := 0;
    end if;
    if (clock'event and clock = '1' and reset = '0') then
      if ((write_enb = '1') and  (full = '0')) then
        ram(w_ptr) := data_in;
        empty <= '0';
        w_ptr:= (w_ptr + 1) mod (16);
        if ( r_ptr = w_ptr ) then
          full <= '1';
        end if;
      end if;

      if ((read_enb = '1') and  (empty = '0')) then
        data_out <= ram(r_ptr);
        full <= '0';
        r_ptr := (r_ptr + 1) mod (16);
        if ( r_ptr = w_ptr ) then
          empty <= '1';
        end if;
      end if;
    end if;
    write_ptr <= w_ptr;
    read_ptr <= r_ptr;
  end process fifo_core;
-- need initialization
end behave;



--*******************************************************************************/
library ieee;
use     std.standard.all;
use     ieee.std_logic_1164.all;
use     ieee.std_logic_unsigned.all;


entity port_fsm is port
		     (clock:           in  std_logic;
		      reset:           in  std_logic;
		      suspend_data_in: out std_logic:='0';
              err:             out std_logic:='0';
		      write_enb:       out std_logic_vector (2 downto 0):= "000";
			  fifo_empty:      in  std_logic;
		      hold:            in  std_logic;
		      packet_valid:    in  std_logic;
		      data_in:         in  std_logic_vector (7 downto 0);
		      data_out:        out std_logic_vector (7 downto 0) := "00000000";
              addr:            out std_logic_vector (1 downto 0) := "00");  
end port_fsm;


architecture behave of port_fsm is
  type state_type is (ADDR_WAIT,DATA_LOAD,PARITY_LOAD,HOLD_STATE,BUSY_STATE);
  signal write_enb_r:          std_logic_vector (2 downto 0):= "000";
  signal fsm_write_enb:        std_logic:='0';
  signal state_r, state:       state_type;
  signal parity:               std_logic_vector (7 downto 0):= "00000000";
  signal parity_delayed:       std_logic_vector (7 downto 0):= "00000000";
  signal sus_data_in:          std_logic:='0';

  signal delay_clock: std_logic := '0';
  signal err_detect : std_logic := '0';
  signal err_delay  : std_logic := '0';

begin
    
  delay_clock <= clock after 25 ns; 

  suspend_data_in <= sus_data_in;

  addr_mux: process (packet_valid, reset)
  begin
    if (reset'event and reset = '1') then
       write_enb_r    <=      "000";
    end if;
    if (packet_valid'event and packet_valid = '1' and reset = '0') then
      case data_in(1 downto 0) is
	when "00" => 
	  write_enb_r(0) <= '1'; 
	  write_enb_r(1) <= '0'; 
	  write_enb_r(2) <= '0'; 
	when "01" => 
	  write_enb_r(0) <= '0'; 
	  write_enb_r(1) <= '1'; 
	  write_enb_r(2) <= '0'; 
	when "10" => 
	  write_enb_r(0) <= '0'; 
	  write_enb_r(1) <= '0'; 
	  write_enb_r(2) <= '1'; 
	when others =>
	  write_enb_r <= ("000"); 
      end case;
    end if;
  end process addr_mux;


  fsm_state: process (clock)
  begin 
    if (clock'event and clock = '1') then
        state_r <= state;
    end if;
  end process fsm_state;

  fsm_core: process (state_r, packet_valid, fifo_empty, hold, data_in, reset) 
  begin
    if (reset'event and reset = '1') then 
      state <= ADDR_WAIT;   
      sus_data_in   <= '0';
      fsm_write_enb <= '0';
      addr          <= "00";
    end if;
    if (reset = '0') then 
	  state <= state_r;   --Default state assignment
      case state_r is
        when ADDR_WAIT =>
          --transition--  
          if ((packet_valid = '1') and 
              ("00" <= data_in(1 downto 0)) and 
              (data_in(1 downto 0) <= "10")) then
			if (fifo_empty = '1') then  
               state <= DATA_LOAD;
			else
			   state <= BUSY_STATE;
			end if;      
          end if;
          --combinational--

          sus_data_in <= not fifo_empty;

          if ((packet_valid = '1') and 
              ("00" <= data_in(1 downto 0)) and 
              (data_in(1 downto 0) <= "10") and
			  (fifo_empty='1')) then
            addr <= data_in(1 downto 0);
            data_out  <= data_in;
            fsm_write_enb <= '1';
		  else
            fsm_write_enb <= '0';
          end if;
          
        when DATA_LOAD =>
          --transition--  
          if ((packet_valid = '1') and
              (hold = '0')) then
            state <= DATA_LOAD;
          elsif ((packet_valid = '0') and 
                 (hold = '0')) then
            state <= PARITY_LOAD;
          else
            state <= HOLD_STATE;
          end if;
          --combinational--
         sus_data_in <= '0';
         if ((packet_valid = '1') and
              (hold = '0')) then
            data_out <= data_in;
            fsm_write_enb <= '1';
          elsif ((packet_valid = '0') and 
                 (hold = '0')) then
            data_out <= data_in;
            fsm_write_enb <= '1';
          else
            fsm_write_enb <= '0';
          end if;
          
        when PARITY_LOAD => 
          --transition--  
          state <= ADDR_WAIT;
          --combinational--
          data_out <= data_in;
          fsm_write_enb <= '0';

        when HOLD_STATE => 
          --transition--  
          if (hold = '1') then
            state <= HOLD_STATE;
          
          elsif (hold = '0' and packet_valid = '0') then
            state <= PARITY_LOAD;

          else
            state <= DATA_LOAD;        
          end if;
          --combinational--
          if (hold = '1') then
            sus_data_in <= '1';
            fsm_write_enb <= '0';
          else 
            fsm_write_enb <= '1';
			data_out <= data_in;
          end if;

        when BUSY_STATE => 
          --transition--  
          if (fifo_empty = '0') then
            state <= BUSY_STATE;        
          else 
            state <= DATA_LOAD;        
          end if;
          --combinational--
          if (fifo_empty = '0') then
            sus_data_in <= '1';
          else
            addr <= data_in(1 downto 0); -- hans
            data_out  <= data_in;
            fsm_write_enb <= '1';
          end if;
       end case;
    end if;
  end process fsm_core;

  write_enb(0) <= write_enb_r(0) and fsm_write_enb;
  write_enb(1) <= write_enb_r(1) and fsm_write_enb;
  write_enb(2) <= write_enb_r(2) and fsm_write_enb;
  
  error_detect: process(clock, reset) 
  begin
    if ((reset'event) and (reset = '1')) then
      err_delay <= '0';         
      err       <= '0';         
    end if;
    if ((clock'event) and (clock='1') and (reset = '0')) then
      err_delay <= err_detect;
      err       <= err_delay;
    end if;
  end process error_detect;
    
  parity_calc: process(delay_clock, reset)            
  begin             
    if ((reset'event) and (reset = '1')) then 
	  parity_delayed <= "00000000";
	  parity         <= "00000000";
	  err_detect     <= '0';     
    end if;
    if ((delay_clock'event) and (delay_clock='0') and (reset = '0')) then 
	  parity_delayed <= parity;
      err_detect <= '0';
      if ((packet_valid = '1') and (sus_data_in='0')) then
        parity <= parity xor data_in;
      elsif (packet_valid = '0') then
	    if ((state   = PARITY_LOAD) and (parity         /= data_in)) then
          err_detect <= '1';
        else
          err_detect <= '0';
	    end if;	
	    parity <= "00000000";
      end if; 
	end if;  
    
  end process parity_calc;
  

end behave;

--*******************************************************************************/
library ieee;

use     std.standard.all;
use     ieee.std_logic_1164.all;
use     ieee.std_logic_unsigned.all;

entity router is
  port
    (clock:                              in  std_logic;
     reset:                              in  std_logic;
     packet_valid:                       in  std_logic;
     data:                               in  std_logic_vector (7 downto 0);
     channel0, channel1, channel2:       out std_logic_vector (7 downto 0);
     vld_chan_0, vld_chan_1, vld_chan_2: out std_logic;
     read_enb_0, read_enb_1, read_enb_2: in  std_logic;
     suspend_data_in:                    out std_logic;
     err:                                out std_logic);
end router;

 
architecture behave of router is 
 

  component fifo
    port 
      (clock:          in std_logic;
       reset:          in std_logic;
       write_enb:      in std_logic;
       read_enb:       in std_logic;
       data_in:        in std_logic_vector (7 downto 0);
       data_out:      out std_logic_vector (7 downto 0);
       empty, full: inout std_logic);
  end component;

  for all: fifo use entity work.fifo (behave);

  component port_fsm
    port
      (clock:           in  std_logic;
       reset:           in  std_logic;
       suspend_data_in: out std_logic;
       err:             out std_logic;
       write_enb:       out std_logic_vector (2 downto 0);
	   fifo_empty:      in  std_logic;
       hold:            in  std_logic;
       packet_valid:    in  std_logic;
       data_in:         in  std_logic_vector (7 downto 0);
       data_out:        out std_logic_vector (7 downto 0);
       addr:            out std_logic_vector (1 downto 0) := "00");  
  end component;

  for all: port_fsm use entity work.port_fsm(behave);

  signal data_out_0, data_out_1, data_out_2:  std_logic_vector (7 downto 0);
  signal write_enb_fsm:      std_logic := '0' ;

  signal full_0,  full_1,  full_2:            std_logic;
  signal empty_0, empty_1, empty_2:           std_logic; 
  signal fifo_empty:                          std_logic;   
  signal fifo_empty0:                         std_logic;   
  signal fifo_empty1:                         std_logic;   
  signal fifo_empty2:                         std_logic;   
  signal hold_0:                         std_logic;   
  signal hold_1:                         std_logic;   
  signal hold_2:                         std_logic;   
  signal hold:                                std_logic;   
  signal write_enb:                           std_logic_vector (2 downto 0) := "000";
  signal write_enb_r:                         std_logic_vector (2 downto 0) := "000";
  signal data_out_fsm:                        std_logic_vector (7 downto 0) := "00000000";

  signal addr:                                std_logic_vector (1 downto 0) := "00";  
begin
  
  --addr <= data (1 downto 0);

  channel0 <= data_out_0;   --mak note assignment only for consistency with vlog env
  channel1 <= data_out_1;
  channel2 <= data_out_2;

  vld_chan_0 <= not empty_0;
  vld_chan_1 <= not empty_1;
  vld_chan_2 <= not empty_2;
  
  fifo_empty0 <= (empty_0 or (    addr(1) or	 addr(0)));     --addr!=00
  fifo_empty1 <= (empty_1 or (    addr(1) or not addr(0)));     --addr!=01
  fifo_empty2 <= (empty_2 or (not addr(1) or	 addr(0)));     --addr!=10

  fifo_empty <= fifo_empty0 and fifo_empty1 and fifo_empty2;
  
  hold_0 <= (full_0 and ( not addr(1) and not addr(0)));     --addr=00
  hold_1 <= (full_1 and ( not addr(1) and     addr(0)));     --addr=01
  hold_2 <= (full_2 and (     addr(1) and not addr(0)));     --addr=10
				
  hold         <= hold_0 or hold_1 or hold_2;
  
  
  queue_0: fifo port map 
    (clock,reset,write_enb(0),read_enb_0,data_out_fsm,data_out_0,empty_0,full_0);
  queue_1: fifo port map 
    (clock,reset,write_enb(1),read_enb_1,data_out_fsm,data_out_1,empty_1,full_1);
  queue_2: fifo port map 
    (clock,reset,write_enb(2),read_enb_2,data_out_fsm,data_out_2,empty_2,full_2);
  
  in_port: port_fsm port map 
    (clock, reset, suspend_data_in, err, write_enb, fifo_empty, hold, packet_valid, data, data_out_fsm, addr);

end behave;


--*******************************************************************************/
library ieee;

use     std.standard.all;
use     ieee.std_logic_1164.all;
use     ieee.std_logic_unsigned.all;

entity top is 
end top;

architecture behave of top is
  component comspec
  end component;
  for all: comspec use entity work.SPECMAN_REFERENCE (arch);

  component router
    port
      (clock:                              in  std_logic;
       reset:                              in  std_logic;
       packet_valid:                       in  std_logic;
       data:                               in  std_logic_vector (7 downto 0);
       channel0, channel1, channel2:       out std_logic_vector (7 downto 0);
       vld_chan_0, vld_chan_1, vld_chan_2: out std_logic;
       read_enb_0, read_enb_1, read_enb_2: in  std_logic;
       suspend_data_in:                    out std_logic;
       err:                                out std_logic);
  end component;

  for all: router use entity work.router (behave);

  signal clock:                              std_logic := '0';
  signal reset:                              std_logic := '0';
  signal packet_valid:                       std_logic := '0';
  signal data:                               std_logic_vector (7 downto 0) := "00000000";
  signal channel0, channel1, channel2:       std_logic_vector (7 downto 0);
  signal vld_chan_0, vld_chan_1, vld_chan_2: std_logic;
  signal suspend_0, suspend_1, suspend_2   : std_logic := '1';
  signal suspend_data_in:                    std_logic;
  signal err:                                std_logic;
 
  signal read_enb_0  : std_logic;
  signal read_enb_1  : std_logic;
  signal read_enb_2  : std_logic;


begin

  I: comspec;

  clock <= not clock after 50 ns;

  read_enb_0 <= not suspend_0;
  read_enb_1 <= not suspend_1;
  read_enb_2 <= not suspend_2;
  
  router1: router port map
    (clock, reset, packet_valid, data, 
     channel0,channel1,channel2,
     vld_chan_0,vld_chan_1,vld_chan_2,
     read_enb_0,read_enb_1,read_enb_2,
     suspend_data_in,err);
    

end behave;

